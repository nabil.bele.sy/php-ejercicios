<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>practica 3 2022</title>
</head>

<body>
    <form action="ex1.php" method="GET">
        <h2>Ejercicio 1</h2>
        <p>Declara dos variables y asignarles dos numeros
            Mostrar en pantalla las dos variables cada una en un div distinto
        </p>
        <br>
        <div>
            <label for="num1">numero1</label>
            <input type="text" id="num1" name="num1" placeholder="numero1" />
        </div>
        <div>
            <label for="num2">numero2</label>
            <input type="text" id="num2" name="num2" placeholder="numero2" />
        </div>
        <input type="submit" value="go">
    </form>
    <hr>
    <form action="ex2.php" method="GET">
        <h2>Ejercicio 2</h2>
        <p>Crear un formulario con 4 cajas de texto
            Realizar un programa que lea el valor de esas 4 cajas de texto y muestre el resultado en div distintos
        </p>
        <br>
        <div>
            <label for="txt1">texto1</label>
            <input type="text" id="txt1" name="txt1" placeholder="texto1" />
        </div>
        <div>
            <label for="txt2">texto2</label>
            <input type="text" id="txt2" name="txt2" placeholder="texto2" />
        </div>
        <div>
            <label for="txt3">texto3</label>
            <input type="text" id="txt3" name="txt3" placeholder="texto3" />
        </div>
        <div>
            <label for="txt4">texto4</label>
            <input type="text" id="txt4" name="txt4" placeholder="texto4" />
        </div>
        <input type="submit" value="go">
    </form>
    <hr>
    <form action="ex3.php" method="GET">
        <h2>Ejercicio 3</h2>
        <p>Realizar un programa que lee un numero y te indica si es negativo o positivo
        </p>
        <br>
        <div>
            <label for="num">numero</label>
            <input type="number" id="num" name="num" placeholder="numero" />
        </div>
        <input type="submit" value="go">
    </form>
    <hr>
    <form action="ex9.php" method="GET">
        <h2>Ejercicio 9</h2>
        <p>Crear una aplicación que nos pida 2 numeros y nos devuelva <br>
            - suma <br>
            - resta <br>
            - producto <br>
            - division
        </p>
        <br>
        <div>
            <label for="num1">numero1</label>
            <input type="number" id="num1" name="num1" placeholder="numero1" />
        </div>
        <div>
            <label for="num2">numero2</label>
            <input type="number" id="num2" name="num2" placeholder="numero2" />
        </div>
        <input type="submit" value="go">
    </form>
    <hr>
    <form action="ex10.php" method="GET">
        <h2>Ejercicio 10</h2>
        Realiza una aplicación que le pasemos una nota y nos devuelva la nota en Texto. Realizarlo mediante if<br>
        - 0 a 3: muy deficiente<br>
        - 3 a 5: suspenso<br>
        - 5 a 6 : suficiente<br>
        - 6 a 7 : bien<br>
        - 7 a 9: notable<br>
        - 9 a 10: Sobresaliente
        </p>
        <br>
        <div>
            <label for="nota">nota</label>
            <input type="number" id="nota" name="nota" placeholder="nota" />
        </div>
        <input type="submit" value="go">
    </form>
    <hr>
</body>

</html>